package pt.unl.fct.di.apdc.individual.util;

public class LoginData {

	public String email;
	public String password;

	public LoginData() {}

	public LoginData(String username, String password) {
		this.email = username;
		this.password = password;
	}

}