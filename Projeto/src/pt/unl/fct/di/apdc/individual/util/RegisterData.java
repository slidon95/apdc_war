package pt.unl.fct.di.apdc.individual.util;

public class RegisterData {
	public String email;
	public String telefone_fixo;
	public String telefone_movel;
	public String morada;
	public String rua;
	public String localidade;
	public String codigo_postal;
	public String nif;
	public String cartao_cidadao;
	public String password;
	public String confirmation;

	public RegisterData() {

	}

	public RegisterData(String email, String telefone_fixo, String telefone_movel ,String morada ,String rua, String localidade, String codigo_postal ,String nif,String cartao_cidadao, String password, String confirmation) {

		this.nif = nif;
		this.telefone_fixo = telefone_fixo;
		this.telefone_movel=telefone_movel;
		this.morada=morada;
		this.rua=rua;
		this.localidade=localidade;
		this.codigo_postal=codigo_postal;
		this.cartao_cidadao=cartao_cidadao;
		this.email = email;
		this.password = password;
		this.confirmation = confirmation;
	}

	private boolean validField(String value) {
		return value != null && !value.equals("");
	}

	public boolean validRegistration() {
		return validField(email) &&
				validField(telefone_fixo) &&
				validField(telefone_movel) &&
				validField(morada) &&
				validField(rua) &&
				validField(localidade) &&
				validField(codigo_postal) &&
				validField(nif) &&
				validField(cartao_cidadao) &&
				validField(password) &&
				validField(confirmation) &&
				password.equals(confirmation) &&
				email.contains("@");		
	}

}