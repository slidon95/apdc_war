package pt.unl.fct.di.apdc.individual.resources;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.apache.commons.codec.digest.DigestUtils;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.Transaction;

import pt.unl.fct.di.apdc.individual.util.LoginData;
import pt.unl.fct.di.apdc.individual.util.RegisterData;

@Path("/registo")
public class RegisterResource {

	/**
	 * A logger object.
	 */
	private static final Logger LOG = Logger.getLogger(RegisterResource.class.getName());
	private static final DatastoreService DATASTORE = DatastoreServiceFactory.getDatastoreService();
public List<String> users = new ArrayList();
	public RegisterResource() {} // Nothing to be done here...

	@POST
	@Path("/v1")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	// Basic code
	public Response doRegistrationV1(LoginData data) {

		Entity user = new Entity("User", data.email);
		user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
		user.setUnindexedProperty("user_creation_time", new Date());
		DATASTORE.put(user);
		LOG.info("User registered " + data.email);
		users.add(data.email);
		return Response.ok("{}").build();

	}


	@POST
	@Path("/v2")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	// Checks that the user exists but may have problems with concurrent transactions
	public Response doRegistrationV2(RegisterData data) {

		if(!data.validRegistration()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.email);
			Entity user = DATASTORE.get(userKey);
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", data.email);
			user.setProperty("user_telefone_fixo", data.telefone_fixo);
			user.setProperty("user_telefone_movel", data.telefone_movel);
			user.setProperty("user_morada", data.morada);
			user.setProperty("user_rua", data.rua);
			user.setProperty("user_localidade", data.localidade);
			user.setProperty("user_codigo_postal", data.codigo_postal);
			user.setProperty("user_nif", data.nif);
			user.setProperty("user_cartao_cidadao", data.cartao_cidadao);
			user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
			user.setProperty("user_email", data.email);
			user.setUnindexedProperty("user_creation_time", new Date());
			DATASTORE.put(user);
			LOG.info("User registered " + data.email);
			users.add(data.email);
			return Response.ok("{}").build();
		}
	}


	@POST
	@Path("/v3")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public Response doRegistrationV3(RegisterData data) {

		if(!data.validRegistration()) {
			return Response.status(Status.BAD_REQUEST).entity("Missing or wrong parameter.").build();
		}

		Transaction txn = DATASTORE.beginTransaction();
		try {
			// If the entity does not exist an Exception is thrown. Otherwise,
			Key userKey = KeyFactory.createKey("User", data.nif);
			Entity user = DATASTORE.get(userKey);
			txn.rollback();
			return Response.status(Status.BAD_REQUEST).entity("User already exists.").build(); 
		} catch (EntityNotFoundException e) {
			Entity user = new Entity("User", data.email);
			user.setProperty("user_telefone_fixo", data.telefone_fixo);
			user.setProperty("user_telefone_movel", data.telefone_movel);
			user.setProperty("user_morada", data.morada);
			user.setProperty("user_rua", data.rua);
			user.setProperty("user_localidade", data.localidade);
			user.setProperty("user_codigo_postal", data.codigo_postal);
			user.setProperty("user_nif", data.nif);
			user.setProperty("user_cartao_cidadao", data.cartao_cidadao);
			user.setProperty("user_pwd", DigestUtils.sha512Hex(data.password));
			user.setProperty("user_email", data.email);
			user.setUnindexedProperty("user_creation_time", new Date());
			DATASTORE.put(txn,user);
			LOG.info("User registered " + data.email);
			txn.commit();
			return Response.ok("{}").build();
		} finally {
			if (txn.isActive()) {
				txn.rollback();
			}
		}	
	}
	
	
	

}