var map;
var geocoder;

function initMap() {    
    geocoder = new google.maps.Geocoder();
    
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 38.659784, lng: -9.202765},
        zoom: 16
        });
    
    /*var latlng = new google.maps.Latlng(38.659784, -9.202765);
    var mapOptions = {
            zoom : 16,
            center: latlng
    }
    map = new google.maps.Map(document.getElementById('map'), mapOptions);
    
    /*var sala116 = new google.maps.LatLng(38.662307, -9.201775);
            
    var marker = new google.maps.Marker({position: sala116, map: map});*/
    
     function codeAddress() {
            var morada = document.getElementById('morada').value;
            geocoder.geocode( { 'morada': morada}, function(results, status) {
              if (status == 'OK') {
                map.setCenter(results[0].geometry.location);
                var marker = new google.maps.Marker({
                    map: map,
                    position: results[0].geometry.location
                });
              } else {
                alert('Geocode was not successful for the following reason: ' + status);
              }
            });
          }

}
