captureData = function(event) {
	var data = $('form[name="registo"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "http://individual-1627.appspot.com/rest/registo/v2",
		//url:"http://localhost:8888/rest/registo/v2",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {
				alert("Got token with id: " + response.tokenID);
				// Store token id for later use in localStorage
				localStorage.setItem('tokenID', response.tokenID);
			}
			else {
				alert("No response");
			}
		},
		error: function(response) {
			alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	var frms = $('form[name="registo"]');     //var frms = document.getElementsByName("registo");
	frms[0].onsubmit = captureData;
}