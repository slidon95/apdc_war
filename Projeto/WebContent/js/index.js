



$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});

$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});

captureData_login = function(event) {
	var data = $ ('form[name="login"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "http://matchbox-apdc.appspot.com/rest/login",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {
				alert("Got token with id: " + response.tokenID);
				// Store token id for later use in localStorage
				localStorage.setItem('tokenID', response.tokenID);
			}
			else {
				alert("No response");
			}
		},
		error: function(response) {
			alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};
captureData_register = function(event) {
	var data = $ ('form[name="register"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "http://matchbox-apdc.appspot.com/rest/register",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {
				alert("Got token with id: " + response.tokenID);
				// Store token id for later use in localStorage
				localStorage.setItem('tokenID', response.tokenID);
			}
			else {
				alert("No response");
			}
		},
		error: function(response) {
			alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	var loginform = $('form[name="login"]');  
	var registerform = $('form[name="register"]'); 
	//var frms = document.getElementsByName("login");

	registerform[0].onsubmit = captureData_register;
	loginform[0].onsubmit = captureData_login;
	}

