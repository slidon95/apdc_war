captureData = function(event) {
	var data = $('form[name="logOut"]').jsonify();
	console.log(data);
	$.ajax({
		type: "POST",
		url: "http://individual-1627.appspot.com/rest/login/logOut",
		//url:"http://localhost:8888/rest/login/logOut",
		contentType: "application/json; charset=utf-8",
		crossDomain: true,
		//dataType: "json",
		success: function(response) {
			if(response) {
				alert("Got token with id: " + response.tokenID);
				// Store token id for later use in localStorage
				localStorage.setItem('tokenID', response.tokenID);
			}
			else {
				alert("No response");
			}
		},
		error: function(response) {
			alert("Error: "+ response.status);
		},
		data: JSON.stringify(data)
	});

	event.preventDefault();
};

window.onload = function() {
	var frms = $('form[name="logOut"]');     //var frms = document.getElementsByName("registo");
	frms[0].onsubmit = captureData;
}